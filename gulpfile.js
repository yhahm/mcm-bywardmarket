"use strict";

var gulp         = require('gulp'),
    concat       = require('gulp-concat'),
    sass         = require('gulp-sass'),
    watch        = require('gulp-watch'),
    autoprefixer = require('gulp-autoprefixer'),
    browserSync  = require('browser-sync').create(),
    uglify       = require('gulp-uglify'),
    htmlPartial  = require('gulp-html-partial'),
    runSequence  = require('run-sequence'),
    inject       = require('gulp-inject-string');


var locations = {
    styles: {
        all: './scss/**/*.scss',
         in: './scss/main.scss',
        out: './files/css',
        filename: 'main.min.css'
    },
    html: {
        all: './*.html',
        partials: './src/partials/*.html',
        out: './dist'
    },
    assets: {
        all: './src/assets/**/*',
        out: './dist/assets'
    },
    img: {
        all: './src/img/**',
        out: './dist/img'
    },
    fonts: {
        all: './src/fonts/**/*',
        out: './dist/fonts'
    },
    js: {
        all: './src/js/**/*.js',
        out: './dist/js'
    }
};


/**
 * Browser sync
 */
gulp.task('bs:reload', function () {
    browserSync.reload();
});


/**
 * Server
 */
gulp.task('server:start', function() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
});


/**
 * Styles
 */
gulp.task('styles:all', function (callback) {
    runSequence('styles:build', 'bs:reload', callback);
});

gulp.task('styles:build', function () {
    return gulp.src(locations.styles.in)
        .pipe(sass({
            outputStyle: 'compressed'
        }).on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(concat(locations.styles.filename))
        .pipe(gulp.dest(locations.styles.out));
});

/*
gulp.task('styles:extra', function() { // NOTE: Safari fixes contain CSS that won't compile with Sass, combine with main file after build
    return gulp.src(['./dist/css/main.min.css', './src/sass/vendor/_safari.css'])
        .pipe(concat(locations.styles.filename))
        .pipe(gulp.dest(locations.styles.out))
        .pipe(browserSync.stream());
})
*/


/**
 * JS
 */
gulp.task('js:uglify', function () {
    return gulp.src(locations.js.all)
        .pipe(uglify())
        .pipe(gulp.dest(locations.js.out));
});


/**
 * Copy
 */
gulp.task('copy:html', function () {
    return gulp.src(locations.html.all)
        .pipe(htmlPartial({
            basePath: './src/partials/'
        }))
        .pipe(inject.replace('%%cache-busting-date%%', Date.now()))
        .pipe(gulp.dest(locations.html.out));
});

gulp.task('copy:assets', function () {
    return gulp.src(locations.assets.all)
        .pipe(gulp.dest(locations.assets.out));
});

gulp.task('copy:img', function () {
    return gulp.src(locations.img.all)
        .pipe(gulp.dest(locations.img.out));
});

gulp.task('copy:fonts', function () {
    return gulp.src(locations.fonts.all)
        .pipe(gulp.dest(locations.fonts.out));
});

gulp.task('copy:server-config', function() {
    return gulp.src('web.config')
        .pipe(gulp.dest(locations.html.out));
});


/**
 * Watchers
 */
gulp.task('watch:html', function () {    
    //gulp.watch([locations.html.all, locations.html.partials], ['copy:html']);
    //gulp.watch([locations.html.all, locations.html.partials]).on('change', browserSync.reload);
    gulp.watch([locations.html.all]).on('change', browserSync.reload);
});

gulp.task('watch:scripts', function () {
    gulp.watch(locations.js.all, ['js:uglify']);
});

gulp.task('watch:styles', function () {
    gulp.watch(locations.styles.all, ['styles:all']);
});


gulp.task('watch:all', [
    'server:start',
    'watch:html',
    //'watch:scripts',
    'watch:styles'
]);

gulp.task('build:production', function(callback) {
    runSequence(['copy:html', 'copy:assets', 'copy:img', 'copy:fonts', 'copy:server-config'], 'js:uglify', 'styles:build', 'styles:extra', callback);
});

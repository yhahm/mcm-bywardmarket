var path = require('path');
var express = require("express");
var app = express();
var favicon = require('serve-favicon');


const bodyParser = require("body-parser");
const validationResult = require('express-validator');
const { matchedData, sanitize } = require('express-validator/filter');

app.use(bodyParser.urlencoded({
    extended: true
}));         

//bodyParser.json(options)
//Parses the text as JSON and exposes the resulting object on req.body.
app.use(bodyParser.json());
app.use(validationResult());
app.use('/jquery', express.static(__dirname + '/node_modules/jquery/dist/'));

console.log(__dirname + '/files/favicon.ico');


//Static files setup
app.use(express.static(__dirname + '/files'))
app.use(express.static(__dirname + '/app'))
app.use(favicon(path.join(__dirname + '/favicon.ico')));

//Template Engine Setup
app.set('view engine')

//Setup port to listen on
app.listen(process.env.PORT || 8000, function () {
    console.log('Example app on port 8000!')
})

//Routes
app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname + '/src/main.html'));
})